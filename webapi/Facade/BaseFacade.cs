﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.SqlClient;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Facade
{
    public class BaseFacade
    {
        private readonly IConfiguration _configuration;
        //public IDbConnection DB { get; private set; }
        public BaseFacade(IConfiguration Configuration)
        {
            _configuration = Configuration;
            //DB = new SqlConnection(_configuration.GetConnectionString("MusicCon"));
        }
        public string GetConnection()
        {
            return _configuration.GetConnectionString("MusicCon");
        }
        public DataTable Query<T>(string sql)
        {
            
            string sqlDataSource = GetConnection();

            DataTable table = new DataTable();
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(sql, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);
                    myReader.Close();
                    myCon.Close();
                }
            }
            return table;
        }
        public int Command(string sql, params object[] args)
        {
            string sqlDataSource = GetConnection();

            int rowAffected=0;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(sql, myCon))
                {
                    if (args.Length != 0)
                    {
                        int counter = 0;
                        foreach (var param in args)
                        {
                            counter++;
                            myCommand.Parameters.AddWithValue(string.Format("@{0}", counter), param);
                        }
                    }
                    rowAffected = myCommand.ExecuteNonQuery();
                    myCon.Close();
                }
            }
            return rowAffected;
        }
    }
}
