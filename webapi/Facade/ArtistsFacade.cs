﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using webapi.Models;

namespace webapi.Facade
{
    public class ArtistsFacade : BaseFacade, IArtistFacade
    {
        string sql;
        public ArtistsFacade(IConfiguration configuration):base(configuration)
        {
            
        }
        public DataTable GetListAll()
        {
            sql = @"select * from Artists";
            DataTable table = Query<Artists>(sql);

            return table;
        }
        public int InsertData(Artists artists)
        {
            sql = @"insert into Artists(ArtistName,AlbumName,ImageURL,ReleaseDate,Price,SampleURL)
                    values (@1,@2,@3,@4,@5,@6)";
            int result = Command(sql, artists.ArtistName, artists.AlbumName, artists.ImageURL, artists.ReleaseDate, artists.Price, artists.SampleURL);

            return result;
        }
        public int UpdateData(Artists artists)
        {
            sql = @"update Artists
                    set ArtistName=@1,AlbumName=@2,ImageURL=@3,ReleaseDate=@4,Price=@5,SampleURL=@6
                    where ArtistID=@7";
            int result = Command(sql, artists.ArtistName, artists.AlbumName, artists.ImageURL, artists.ReleaseDate, artists.Price, artists.SampleURL,artists.ArtistID);

            return result;
        }
        public int DeleteData(int artistsID)
        {
            sql = @"delete from Artists
                    where ArtistID=@1";
            int result = Command(sql, artistsID);

            return result;
        }

    }
}
