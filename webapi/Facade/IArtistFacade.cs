﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using webapi.Models;

namespace webapi.Facade
{
    public interface IArtistFacade
    {
        DataTable GetListAll();
        int InsertData(Artists artists);
        int UpdateData(Artists artists);
        int DeleteData(int artists);
    }
}
