﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using webapi.Facade;
using webapi.Models;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArtistsController : ControllerBase
    {
        private readonly IArtistFacade artistsFacade;
        public ArtistsController(IArtistFacade _artistsFacade)
        {
            artistsFacade = _artistsFacade; 
        }

        [HttpGet]
        public JsonResult Get()
        {
            return new JsonResult(artistsFacade.GetListAll());
        }
        [HttpPost]
        public JsonResult Post(Artists artists)
        {
            return new JsonResult(artistsFacade.InsertData(artists));
        }
        [HttpPut]
        public JsonResult Put(Artists artists)
        {
            return new JsonResult(artistsFacade.UpdateData(artists));
        }
        [HttpDelete]
        public JsonResult Delete(int artistsID)
        {
            return new JsonResult(artistsFacade.DeleteData(artistsID));
        }
    }
}
