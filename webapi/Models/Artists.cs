﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapi.Models
{
    public class Artists
    {
        public int ArtistID { get; set; }
        public string ArtistName { get; set; }
        public string AlbumName { get; set; }
        public string ImageURL { get; set; }
        public DateTime ReleaseDate { get; set; }
        public double Price { get; set; }
        public string SampleURL { get; set; }
    }
}
